# 拉取nginx鏡像
FROM nginx:latest

COPY ["docker/build_config/nginx/nginx.conf", "/etc/nginx/nginx.conf"]

# sites-available目錄(放我們的conf檔)
# 和sites-enabled目錄(透過ln將需要的conf檔從sites-available軟連結過來)
COPY ["docker/build_config/nginx/web.conf", "/etc/nginx/sites-available/web.conf"]

# 建立/usr/share/nginx/html/static，是為了之後能將django的靜態檔volume進來
RUN mkdir -p /etc/nginx/sites-enabled/ \
    && ln -s /etc/nginx/sites-available/web.conf /etc/nginx/sites-enabled/ \
    && mkdir -p /usr/share/nginx/html/static

CMD ["nginx", "-g", "daemon off;"]