#!/bin/bash

# 將Django的靜態檔蒐集至static_root(settings.py配置)中
echo "Collect static files"
python /package/webapi_2/manage.py collectstatic --noinput

# 還不清楚作用(好像是會去執行之後的CMD指令)
exec "$@"