django ~= 3.2.0
djangorestframework ~= 3.13.1
django-filter ~= 21.0
jsonfield ~= 3.1.0
mysqlclient ~= 2.1.0
django-mysql ~= 4.5.0
PyYAML ~= 6.0
requests ~= 2.27.1
Django-environ ~= 0.8.1
PyJWT ~= 2.3.0
uwsgi