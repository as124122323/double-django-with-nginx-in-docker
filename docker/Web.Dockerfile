# 拉取Python映像檔
FROM python:3.9

# 設定工作目錄，如果該目錄不存在的話會自動建立
# 必須將WORKDIR設為Django專案根目錄，不然之後用uwsgi運行web後，訪問web時會報500 error
WORKDIR /package/webapi
# 將設定檔複製至對應位置
COPY ["docker/build_config", "/package/build_config"]
# 因為RUN的默認shell為"/bin/sh"，故要先將他轉為"/bin/bash"，才可以使用source命令
SHELL ["/bin/bash", "-c"]
# 安裝python套件
RUN pip install -r /package/build_config/python/webapi/requirements.txt

# 將Django專案複製進容器
COPY ["www/webapi", "/package/webapi"]
RUN chmod -R 777 /package

ENTRYPOINT ["/bin/bash", "/package/build_config/docker/webapi/entrypoint.sh"]
CMD python /package/webapi/manage.py runserver 0.0.0.0:8000