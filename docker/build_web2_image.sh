#!/usr/bin/bash

this_file_path=`readlink -f $0`
this_dir_path=`dirname $this_file_path`
project_dir_path=`dirname $this_dir_path`

echo "Project Path:" $project_dir_path
cd $project_dir_path

sudo docker build \
-f docker/Web2.Dockerfile \
-t uwsgi_webapi_2:22.5.1 \
.
